$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/techGlobalPagination.feature");
formatter.feature({
  "name": "TechGlobal Pagination Functionalities",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Regression"
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user navigates to \"https://techglobal-training.netlify.app/\"",
  "keyword": "Given "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userNavigatesTo(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user moves to \"Practices\" header dropdown",
  "keyword": "When "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userMovesToHeaderDropdown(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user clicks on \"Frontend Testing\" header dropdown option",
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userClicksOnHeaderDropdownOption(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be navigated to \"https://techglobal-training.netlify.app/frontend\"",
  "keyword": "Then "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldBeNavigatedTo(String)"
});
formatter.result({
  "error_message": "org.junit.ComparisonFailure: expected:\u003c...raining.netlify.app/[back]end\u003e but was:\u003c...raining.netlify.app/[front]end\u003e\n\tat org.junit.Assert.assertEquals(Assert.java:115)\n\tat org.junit.Assert.assertEquals(Assert.java:144)\n\tat Steps.TechGlobalPaginationBaseSteps.userShouldBeNavigatedTo(TechGlobalPaginationBaseSteps.java:50)\n\tat ✽.user should be navigated to \"https://techglobal-training.netlify.app/frontend\"(src/test/resources/features/techGlobalPagination.feature:8)\n",
  "status": "failed"
});
formatter.step({
  "name": "user clicks on \"Pagination\" card",
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userClicksOnCard(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should be navigated to \"https://techglobal-training.netlify.app/frontend/pagination\"",
  "keyword": "Then "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldBeNavigatedTo(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Validate main content of pagination page",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "name": "user should see \"Pagination\" heading",
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldSeeHeading(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should see \"World City Populations 2022\" heading",
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldSeeHeading(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should see \"What are the most populated cities in the world? Here is a list of the top five most populated cities in the world:\" paragraph",
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldSeeParagraph(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user navigates to \"https://techglobal-training.netlify.app/\"",
  "keyword": "Given "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userNavigatesTo(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user moves to \"Practices\" header dropdown",
  "keyword": "When "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userMovesToHeaderDropdown(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user clicks on \"Frontend Testing\" header dropdown option",
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userClicksOnHeaderDropdownOption(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be navigated to \"https://techglobal-training.netlify.app/frontend\"",
  "keyword": "Then "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldBeNavigatedTo(String)"
});
formatter.result({
  "error_message": "org.junit.ComparisonFailure: expected:\u003c...raining.netlify.app/[back]end\u003e but was:\u003c...raining.netlify.app/[front]end\u003e\n\tat org.junit.Assert.assertEquals(Assert.java:115)\n\tat org.junit.Assert.assertEquals(Assert.java:144)\n\tat Steps.TechGlobalPaginationBaseSteps.userShouldBeNavigatedTo(TechGlobalPaginationBaseSteps.java:50)\n\tat ✽.user should be navigated to \"https://techglobal-training.netlify.app/frontend\"(src/test/resources/features/techGlobalPagination.feature:8)\n",
  "status": "failed"
});
formatter.step({
  "name": "user clicks on \"Pagination\" card",
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userClicksOnCard(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should be navigated to \"https://techglobal-training.netlify.app/frontend/pagination\"",
  "keyword": "Then "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldBeNavigatedTo(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Validate pagination page next/previous buttons",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "name": "user should see \"Previous\" button is disabled",
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldSeeButtonIsDisabled(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should see \"Next\" button is enabled",
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldSeeButtonIsEnabled(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user clicks on \"Next\" button",
  "keyword": "When "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userClicksOnButton(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should see \"Previous\" button is enabled",
  "keyword": "Then "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldSeeButtonIsEnabled(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user clicks on \"Next\" button till it becomes disabled",
  "keyword": "When "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userClicksOnButtonTillItBecomesDisabled(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should see \"Previous\" button is enabled",
  "keyword": "Then "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldSeeButtonIsEnabled(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should see \"Next\" button is disabled",
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldSeeButtonIsDisabled(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.embedding("image/png", "embedded1.png");
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user navigates to \"https://techglobal-training.netlify.app/\"",
  "keyword": "Given "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userNavigatesTo(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user moves to \"Practices\" header dropdown",
  "keyword": "When "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userMovesToHeaderDropdown(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user clicks on \"Frontend Testing\" header dropdown option",
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userClicksOnHeaderDropdownOption(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user should be navigated to \"https://techglobal-training.netlify.app/frontend\"",
  "keyword": "Then "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldBeNavigatedTo(String)"
});
formatter.result({
  "error_message": "org.junit.ComparisonFailure: expected:\u003c...raining.netlify.app/[back]end\u003e but was:\u003c...raining.netlify.app/[front]end\u003e\n\tat org.junit.Assert.assertEquals(Assert.java:115)\n\tat org.junit.Assert.assertEquals(Assert.java:144)\n\tat Steps.TechGlobalPaginationBaseSteps.userShouldBeNavigatedTo(TechGlobalPaginationBaseSteps.java:50)\n\tat ✽.user should be navigated to \"https://techglobal-training.netlify.app/frontend\"(src/test/resources/features/techGlobalPagination.feature:8)\n",
  "status": "failed"
});
formatter.step({
  "name": "user clicks on \"Pagination\" card",
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userClicksOnCard(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should be navigated to \"https://techglobal-training.netlify.app/frontend/pagination\"",
  "keyword": "Then "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldBeNavigatedTo(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Validate the pagination page cities content",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Regression"
    }
  ]
});
formatter.step({
  "name": "user should see \"Tokyo\" city with info below and an image",
  "rows": [
    {
      "cells": [
        "City: Tokyo",
        "Country: Japan",
        "Population: 37,435,191"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldSeeCityWithInfoBelowAndAnImage(DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should see \"Delhi\" city with info below and an image",
  "rows": [
    {
      "cells": [
        "City: Delhi",
        "Country: India",
        "Population: 29,399,141"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldSeeCityWithInfoBelowAndAnImage(DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should see \"Shangai\" city with info below and an image",
  "rows": [
    {
      "cells": [
        "City: Shangai",
        "Country: China",
        "Population: 26,317,104"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldSeeCityWithInfoBelowAndAnImage(DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should see \"Sao Paulo\" city with info below and an image",
  "rows": [
    {
      "cells": [
        "City: Sao Paulo",
        "Country: Brasil",
        "Population: 21,846,507"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldSeeCityWithInfoBelowAndAnImage(DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "user should see \"Mexico City\" city with info below and an image",
  "rows": [
    {
      "cells": [
        "City: Mexico City",
        "Country: Mexico",
        "Population: 21,671,908"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "TechGlobalPaginationBaseSteps.userShouldSeeCityWithInfoBelowAndAnImage(DataTable)"
});
formatter.result({
  "status": "skipped"
});
formatter.embedding("image/png", "embedded2.png");
formatter.after({
  "status": "passed"
});
});