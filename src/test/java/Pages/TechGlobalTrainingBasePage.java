package Pages;

import Utils.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class TechGlobalTrainingBasePage {

    public TechGlobalTrainingBasePage(){
        PageFactory.initElements(Driver.getDriver(), this);
    }

    // any common elements from header or footer are here

    @FindBy(id = "logo")
    public WebElement logo;

    @FindBy(id = "dropdown-button")
    public WebElement practicesDropdown;

    @FindBy(id = "dropdown-menu")
    public List<WebElement> practicesDropdownOptions;
}
