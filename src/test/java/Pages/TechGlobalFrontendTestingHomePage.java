package Pages;

import Utils.Driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class TechGlobalFrontendTestingHomePage extends TechGlobalTrainingBasePage{

    public TechGlobalFrontendTestingHomePage(){
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(css = "div[id^='card']")
    public List<WebElement> cards;

    public void getFrontendTestingPage(){
        practicesDropdown.click();
        practicesDropdownOptions.get(0).click();
    }

    public void clickOnCard(String cardText){
        for(WebElement card : cards){
            if(card.getText().equals(cardText)){
                card.click();
                break;
            }
        }
    }
}
