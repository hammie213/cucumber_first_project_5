package Steps;

import Pages.TechGlobalFrontendTestingHomePage;
import Pages.TechGlobalPaginationPage;
import Utils.Driver;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.junit.Assert;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebDriver;

import java.util.List;

public class TechGlobalPaginationBaseSteps {

    WebDriver driver;
    TechGlobalFrontendTestingHomePage techGlobalFrontendTestingHomePage;
    TechGlobalPaginationPage techGlobalPaginationPage;

    @Before
    public void setup(){
        driver = Driver.getDriver();
        techGlobalFrontendTestingHomePage = new TechGlobalFrontendTestingHomePage();
        techGlobalPaginationPage = new TechGlobalPaginationPage();
    }


    @Given("user navigates to {string}")
    public void userNavigatesTo(String url) {
        System.out.println(url);
        driver.get(url);
    }

    @When("user moves to {string} header dropdown")
    public void userMovesToHeaderDropdown(String header) {
        techGlobalFrontendTestingHomePage.practicesDropdown.click();
    }

    @And("user clicks on {string} header dropdown option")
    public void userClicksOnHeaderDropdownOption(String header) {
        techGlobalFrontendTestingHomePage.getFrontendTestingPage();
    }

    @Then("user should be navigated to {string}")
    public void userShouldBeNavigatedTo(String url) {
        Assert.assertEquals(driver.getCurrentUrl(), url);
    }

    @And("user clicks on {string} card")
    public void userClicksOnCard(String card) {
        techGlobalFrontendTestingHomePage.clickOnCard(card);
    }

    @And("user should see {string} heading")
    public void userShouldSeeHeading(String heading) {
        switch (heading){
            case "Pagination":
                Assert.assertEquals(heading, techGlobalPaginationPage.headerText.getText());
                break;
            case "World City Populations 2022":
                Assert.assertEquals(heading, techGlobalPaginationPage.subheadingText.getText());
            default:
                throw new NotFoundException();
        }
    }

//    @And("user should see \"What are the most populated cities in the world? Here is a list of the top five most populated cities in the world:\" paragraph")
//    public void userShouldSeeWhatAreTheMostPopulatedCitiesInTheWorldHereIsAListOfTheTopFiveMostPopulatedCitiesInTheWorldParagraph(String paragraph) {
//        Assert.assertEquals(paragraph, techGlobalPaginationPage.contentText.getText());
//    }

    @And("user should see {string} paragraph")
    public void userShouldSeeParagraph(String paragraph) {
        Assert.assertEquals(paragraph, techGlobalPaginationPage.contentText.getText());
    }

    @And("user should see {string} button is disabled")
    public void userShouldSeeButtonIsDisabled(String button) {
        switch (button){
            case "Previous":
                Assert.assertFalse(techGlobalPaginationPage.previousButton.isEnabled());
                break;
            case "Next":
                Assert.assertFalse(techGlobalPaginationPage.nextButton.isEnabled());
                break;
            default:
                throw new NotFoundException();
        }
    }

    @And("user should see {string} button is enabled")
    public void userShouldSeeButtonIsEnabled(String button) {
        switch (button){
            case "Previous":
                Assert.assertTrue(techGlobalPaginationPage.previousButton.isEnabled());
                break;
            case "Next":
                Assert.assertTrue(techGlobalPaginationPage.nextButton.isEnabled());
                break;
            default:
                throw new NotFoundException();
        }
    }

    @When("user clicks on {string} button")
    public void userClicksOnButton(String button) {
        switch (button){
            case "Previous":
                techGlobalPaginationPage.previousButton.click();
                break;
            case "Next":
                techGlobalPaginationPage.nextButton.click();
                break;
            default:
                throw new NotFoundException();
        }
    }

    @When("user clicks on {string} button till it becomes disabled")
    public void userClicksOnButtonTillItBecomesDisabled(String nextButton){
        while (techGlobalPaginationPage.nextButton.isEnabled()) techGlobalPaginationPage.nextButton.click();
    }

    @And("user should see {string} city with info below and an image")
    public void userShouldSeeCityWithInfoBelowAndAnImage(DataTable dataTable) {
        List<String> expectedText = dataTable.asList();

        for(int i = 0; i < expectedText.size(); i++){
            Assert.assertEquals(expectedText.get(i), techGlobalPaginationPage.cityInfo.get(i).getText());
            Assert.assertTrue(techGlobalPaginationPage.cityImage.isDisplayed());
        }
        if(techGlobalPaginationPage.nextButton.isEnabled()) techGlobalPaginationPage.nextButton.click();
    }
}
